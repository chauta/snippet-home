<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="c-list1">
	<div class="c-imgtext1">
		<div class="c-imgtext1__img">
			<img src="assets/img/common/image1.jpg" alt="" width="370" height="215" >
		</div>
		<div class="c-imgtext1__text">
			<div class="c-imgtext1__title">肥沃な大地で生産する安全な農作物</div>
			<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
		</div>
	</div>
	<div class="c-imgtext1">
		<div class="c-imgtext1__img">
			<img src="assets/img/common/image2.jpg" alt="" width="370" height="215" >
		</div>
		<div class="c-imgtext1__text">
			<div class="c-imgtext1__title">北海道を代表する高品質の牛乳</div>
			<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
		</div>
	</div>
	<div class="c-imgtext1">
		<div class="c-imgtext1__img">
			<img src="assets/img/common/image3.jpg" alt="" width="370" height="215" >
		</div>
		<div class="c-imgtext1__text">
			<div class="c-imgtext1__title">信頼の「鹿追ブランド」牛肉・豚肉</div>
			<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
		</div>
	</div>
</div>
<div class="c-dev-title2">c-list2</div>
<div class="c-list2">
	<div class="c-imgtext2">
		<a href="#">
			<div class="c-imgtext2__img">
				<img src="assets/img/common/image7.jpg" alt="" width="370" height="215" >
			</div>
			<div class="c-imgtext2__text">
				<div class="c-title02">
					<p class="c-title02__sub">Aコープ鹿追店</p>
					<p class="c-title02__title">国産野菜統一宣言！ 地産地消をおいしく応援中。</p>
				</div>
				<p class="c-imgtext2__txt">Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>安心で安全な食品をご提供いたします。</p>
			</div>
		</a>
	</div>
	<div class="c-imgtext2">
		<a href="#">
			<div class="c-imgtext2__img">
				<img src="assets/img/common/image8.jpg" alt="" width="370" height="215" >
			</div>
			<div class="c-imgtext2__text">
				<div class="c-title02">
					<p class="c-title02__sub">ふるさと納税返礼品［鹿追町］</p>
					<p class="c-title02__title">JA鹿追町のふるさとチョイス。</p>
				</div>
				<p class="c-imgtext2__txt">安心、安全な鹿追産の牛肉、豚肉、ハンバーグをご提供いたします。<br>是非ご利用ください。</p>
			</div>
		</a>
	</div>
	<div class="c-imgtext2">
		<a href="#">
			<div class="c-imgtext2__img">
			<img src="assets/img/common/image9.jpg" alt="" width="370" height="215" >
		</div>
		<div class="c-imgtext2__text">
			<div class="c-title02">
				<p class="c-title02__sub">農業求人情報<span>NEW</span></p>
				<p class="c-title02__title">酪農スタッフ募集のお知らせ。</p>
			</div>
			<p class="c-imgtext2__txt">酪農スタッフを募集しています。<br>広大な十勝平野で私たちと一緒に働きませんか！</p>
		</div>
		</a>
	</div>
	<div class="c-imgtext2">
		<a href="#">
			<div class="c-imgtext2__img">
				<img src="assets/img/common/image10.jpg" alt="" width="370" height="215" >
			</div>
			<div class="c-imgtext2__text">
				<div class="c-title02">
					<p class="c-title02__sub">とっておきのレシピ<span>NEW</span></p>
					<p class="c-title02__title">野菜の大根おろしあえ vol.89</p>
				</div>
				<p class="c-imgtext2__txt">旬の野菜を大根おろしであえ特性タレをかけました。<br>ごはんのおかずにもビールのおつまににも良く合います。</p>
			</div>
		</a>
	</div>
	<div class="c-imgtext2">
		<a href="#">
			<div class="c-imgtext2__img">
				<img src="assets/img/common/image11.jpg" alt="" width="370" height="215" >
			</div>
			<div class="c-imgtext2__text">
				<div class="c-title02">
					<p class="c-title02__sub">JA通信しかおい</p>
					<p class="c-title02__title">JA通信11月号を公開しました。</p>
				</div>
				<p class="c-imgtext2__txt">農業ニュースや各種イベントなど楽しい話題をお届けいたします。<br>鹿追町内の皆さまへ毎月無料配布をおこなっています。</p>
			</div>
		</a>
	</div>
</div>
