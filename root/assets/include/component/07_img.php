<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext1</div>
<div class="c-imgtext1">
	<div class="c-imgtext1__img">
		<img src="assets/img/common/image4.jpg" alt="" width="370" height="215" >
	</div>
	<div class="c-imgtext1__text">
		<div class="c-imgtext1__title">青年部</div>
		<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext2</div>
<div class="c-imgtext2">
	<a href="#">
		<div class="c-imgtext2__img">
			<img src="assets/img/common/image7.jpg" alt="" width="370" height="215" >
		</div>
		<div class="c-imgtext2__text">
			<div class="c-title02">
				<p class="c-title02__sub">Aコープ鹿追店</p>
				<p class="c-title02__title">国産野菜統一宣言！ 地産地消をおいしく応援中。</p>
			</div>
			<p class="c-imgtext2__txt">Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br>安心で安全な食品をご提供いたします。</p>
		</div>
	</a>
</div>
